window.addEventListener('load', function(){
  // functions to show all tags and sort in a sub-window
  doc.addEventListener('click', function (event) {
    var target = event.target
    // toggle show all tags
    const postTagsWrapper = document.querySelector(`.${classTaxosOpenWidget}`);
    target = target === null ? postTagsWrapper : target;
    const showingAllTags = target.matches(`.${classTaxosOpenWidget}`);
    const isExpandButton = target.matches(`.${classTaxosShowAllButton}`);
    const isCloseButton = target.matches(`.${classTaxosShowHideButton}`) || target.closest(`.${classTaxosShowHideButton}`);
    const isButton = isExpandButton || isCloseButton;
    const isActionable = isButton || showingAllTags;
    if (isActionable) {
      if (isButton) {
        if (isExpandButton) {
          pushClass(target.nextElementSibling, classTaxosOpenWidget);
        } else {
          deleteClass(postTagsWrapper, classTaxosOpenWidget);
        }
      } else {
        isActionable ? deleteClass(target, classTaxosOpenWidget) : false;
      }
    }
    // sorting tags
    const active = 'active';
    if (target.matches('.' + classTaxosSortButton) || target.matches('.' + classTaxosSortButton + ' span')) {
      const tagsList = target.closest('.' + classTaxosList);
      const sortButton = tagsList.querySelector('.' + classTaxosSortButton);
      modifyClass(sortButton, 'sorted');
      const tags = tagsList.querySelectorAll('.' + classTaxosTags);
      Array.from(tags).forEach(function (tag) {
        const order = tag.dataset.position;
        const reverseSorting = containsClass(tag, active);
        tag.style.order = reverseSorting ? 0 : -order;
        modifyClass(tag, active);
      })
    }
  })
});
