// sets the customization to boolean value (or switch if no value given) - table of content
function setCustomizationTocSwitch(status) {
  const withoutToc = document.querySelector('.' + classFrameWithoutToc);
  const withToc = document.querySelector('.' + classFrameWithToc);
  if (typeof status == "undefined" || typeof status != "boolean") {
    if (withToc) {
      deleteClass(withToc, classFrameWithToc);
      pushClass(withToc, classFrameWithoutToc);
      setStorValue(storKeyToc, storValueOffToc);
    } else if (withoutToc) {
      deleteClass(withoutToc, classFrameWithoutToc);
      pushClass(withoutToc, classFrameWithToc);
      setStorValue(storKeyToc, storValueOnToc);
    } else {
      // current status not found
    }
  } else if (status == true) {
    deleteClass(withoutToc, classFrameWithoutToc);
    pushClass(withoutToc, classFrameWithToc);
    setStorValue(storKeyToc, storValueOnToc);
  } else if (status == false) {
    deleteClass(withToc, classFrameWithToc);
    pushClass(withToc, classFrameWithoutToc);
    setStorValue(storKeyToc, storValueOffToc);
  } else {
    // do nothing
  }
}
// sets the button value to actual value - table of content
function setCustomizationTocButton(status) {
  const buttonSwitchToc = document.querySelector('.' + dummyClassSwitchToc);
  if (buttonSwitchToc ){
    if (typeof status == "undefined" || (typeof status != "boolean" && typeof status != "string")) {
      // auto-detect, gets from current status
      const withoutToc = document.querySelector('.' + classFrameWithoutToc);
      const withToc = document.querySelector('.' + classFrameWithToc);
      if (withToc) {
        pushClass(buttonSwitchToc, classButtonAnimate);
      } else if (withoutToc) {
        deleteClass(buttonSwitchToc, classButtonAnimate);
      } else {
        // not found
      }
    } else if (status == true) {
      pushClass(buttonSwitchToc, classButtonAnimate);
    } else if (status == false) {
      deleteClass(buttonSwitchToc, classButtonAnimate);
    } else if (status.toLowerCase() == 'auto' || status.toLowerCase() == 'switch' || status.toLowerCase() == 'invert') {
      // set the button to inverse value.
      buttonSwitchToc.matches('.' + classButtonAnimate) ? deleteClass(buttonSwitchToc, classButtonAnimate) : pushClass(buttonSwitchToc, classButtonAnimate);
    } else {
      // do nothing
    }
  }
}
// init at first launch or page refresh
function initCustomizationTocButton() {
  if (getStorValue(storKeyToc) == storValueOffToc) {
    setCustomizationTocButton(false);
    setCustomizationTocSwitch(false);
  } else if (getStorValue(storKeyToc) == storValueOnToc) {
    setCustomizationTocButton(true);
    setCustomizationTocSwitch(true);
  } else {
    setCustomizationTocButton(storValueDefaultToc);
    setCustomizationTocSwitch(storValueDefaultToc);
  }
}

// sets the customization to boolean value (or switch if no value given) - heading numbering
function setCustomizationHNumSwitch(status){
  const hnumNormal = document.querySelector('.' + classHNumNormal);
  const hnumNumbered = document.querySelector('.' + classHNumNumbered);
  const tocJs = document.getElementsByClassName(dummyClassSwitchHNumtoc);
  if (typeof status == "undefined" || typeof status != 'boolean' ) {
    if (hnumNumbered) {
      deleteClass(hnumNumbered, classHNumNumbered);
      setStorValue(storKeyHNum, storValueOffHNum);
      // do the same for toc
      if (tocJs && tocJs.length > 0) {
        var tocRoot = tocJs.item(0).firstElementChild;
        deleteClass(tocRoot, classHNumNumbtoc);
      }
    } else if (hnumNormal) {
      pushClass(hnumNormal, classHNumNumbered);
      setStorValue(storKeyHNum, storValueOnHNum);
      // do the same for toc
      if (tocJs && tocJs.length > 0) {
        var tocRoot = tocJs.item(0).firstElementChild;
        pushClass(tocRoot, classHNumNumbtoc);
      }
    } else {
      // current status not found
    }
  } else if ( status == true ) {
    if (!hnumNumbered || hnumNumbered == undefined){
      pushClass(hnumNormal, classHNumNumbered);
      // do the same for toc
      if(tocJs && tocJs.length>0){
        const tocRoot = tocJs.item(0).firstElementChild;
        pushClass(tocRoot, classHNumNumbtoc);
      }
    }
    setStorValue(storKeyHNum, storValueOnHNum);
  } else if (status == false ) {
    if (hnumNumbered){
      deleteClass(hnumNumbered, classHNumNumbered);
      // do the same for toc
      if (tocJs && tocJs.length > 0) {
        const tocRoot = tocJs.item(0).firstElementChild;
        deleteClass(tocRoot, classHNumNumbtoc);
      }
    }
    setStorValue(storKeyHNum, storValueOffHNum);
  } else {    
    // do nothing
  }  
}
// sets the button value to actual value - heading numbering
function setCustomizationHNumButton(status){
  const buttonSwitchHNum = document.querySelector('.' + dummyClassSwitchHNumbered);
  if (buttonSwitchHNum) {
    if (typeof status == "undefined" || (typeof status != "boolean" && typeof status != "string")) {
      // auto-detect, gets from current status
      const hnumNumbered = document.querySelector('.' + classHNumNumbered);
      if (hnumNumbered) {
        pushClass(buttonSwitchHNum, classButtonAnimate);
      } else {
        deleteClass(buttonSwitchHNum, classButtonAnimate);
      }
    } else if (status == true) {
      pushClass(buttonSwitchHNum, classButtonAnimate);
    } else if (status == false) {
      deleteClass(buttonSwitchHNum, classButtonAnimate);
    } else if (status.toLowerCase() == 'auto' || status.toLowerCase() == 'switch' || status.toLowerCase() == 'invert') {
      // set the button to inverse value.
      buttonSwitchHNum.matches('.' + classButtonAnimate) ? deleteClass(buttonSwitchHNum, classButtonAnimate) : pushClass(buttonSwitchHNum, classButtonAnimate);
    } else {
      // do nothing
    }
  }
}
// init at first launch or page refresh
function initCustomizationHNumButton() {
  if (getStorValue(storKeyHNum) == storValueOffHNum) {
    setCustomizationHNumButton(false);
    setCustomizationHNumSwitch(false);
  } else if (getStorValue(storKeyHNum) == storValueOnHNum) {
    setCustomizationHNumButton(true);
    setCustomizationHNumSwitch(true);
  } else {
    setCustomizationHNumButton(storValueDefaultHNum);
    setCustomizationHNumSwitch(storValueDefaultHNum);    
  }
}

// sets the customization to boolean value (or switch if no value given) - tables collapsed
function setCustomizationTableSwitch(status){
  const tables = document.querySelector('.' + dummyClassPlaceholderTable);
  const tablesCollapsed = document.querySelector('.' + classTableCollapse);
  if (typeof status == "undefined" || typeof status != "boolean") {
    if (tablesCollapsed) {
      deleteClass(tables, classTableCollapse);
      setStorValue(storKeyTable, storValueOffTable);
    } else if (tables) {
      pushClass(tables, classTableCollapse);
      setStorValue(storKeyTable, storValueOnTable);
    } else {
      // invert
      getStorValue(storKeyTable) == storValueOffTable ? setStorValue(storKeyTable, storValueOnTable) : setStorValue(storKeyTable, storValueOffTable);
    }
  } else if (status == true) {
    if (! tablesCollapsed){
      pushClass(tables, classTableCollapse);
    }
    setStorValue(storKeyTable, storValueOnTable);
  } else if (status == false) {
    if (tablesCollapsed){
      deleteClass(tables, classTableCollapse);
    }
    setStorValue(storKeyTable, storValueOffTable);
  } else {
    // do nothing
  }  
}
// sets the button value to actual value - tables collapsed
function setCustomizationTableButton(status){
  const buttonSwitchTable = document.querySelector('.' + dummyClassSwitchTable);
  if (buttonSwitchTable) {
    if (typeof status == "undefined" || (typeof status != "boolean" && typeof status != "string") ) {
      // auto-detect, gets from current status
      const tables = document.querySelector('.' + dummyClassPlaceholderTable);
      const tablesCollapsed = document.querySelector('.' + classTableCollapse);
      if (tablesCollapsed) {
        pushClass(buttonSwitchTable, classButtonAnimate);
      } else if (tables) {
        deleteClass(buttonSwitchTable, classButtonAnimate);
      } else {
        // no tables in the page
      }
    } else if (status == true) {
      pushClass(buttonSwitchTable, classButtonAnimate);
    } else if (status == false) {
      deleteClass(buttonSwitchTable, classButtonAnimate);
    } else if ( status.toLowerCase() == 'auto' || status.toLowerCase() == 'switch' || status.toLowerCase() == 'invert' ) {
      // set the button to inverse value.
      buttonSwitchTable.matches('.' + classButtonAnimate) ? deleteClass(buttonSwitchTable, classButtonAnimate) : pushClass(buttonSwitchTable, classButtonAnimate);
    } else {
      // do nothing
    }
  }
}
// init at first launch or page refresh
function initCustomizationTableButton(){
  if (getStorValue(storKeyTable) == storValueOffTable){
    setCustomizationTableButton(false);
    setCustomizationTableSwitch(false);
  } else if (getStorValue(storKeyTable) == storValueOnTable) {
    setCustomizationTableButton(true);
    setCustomizationTableSwitch(true);
  } else {
    setCustomizationTableButton(storValueDefaultTable);
    setCustomizationTableSwitch(storValueDefaultTable);
  }
}

// call init when window is loaded and not before all the html gets built.
window.addEventListener('load', function(){
  initCustomizationTocButton();
  initCustomizationHNumButton();
  initCustomizationTableButton();
})

// add the individual listeners for each button
window.addEventListener('load', function () {
  const tocButton = document.getElementsByClassName(dummyClassSwitchToc);
  if(tocButton && tocButton.length>0){
    tocButton.item(0).addEventListener('click', function(){
      setCustomizationTocSwitch();
      setCustomizationTocButton("switch");
    })
  };

  const hnumButton = document.getElementsByClassName(dummyClassSwitchHNumbered);
  if (hnumButton && hnumButton.length > 0){
    hnumButton.item(0).addEventListener('click', function(){
      setCustomizationHNumSwitch();
      setCustomizationHNumButton("switch");
    })
  };

  const tableButton = document.getElementsByClassName(dummyClassSwitchTable);
  if (tableButton && tableButton.length > 0){
    tableButton.item(0).addEventListener('click', function(){
      setCustomizationTableSwitch();
      setCustomizationTableButton("switch");
    })
  };
})
