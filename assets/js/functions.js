function isObj(obj) {
  return (obj && typeof obj === 'object' && obj !== null) ? true : false;
}

function pushClass(el, targetClass) {
  if (isObj(el) && targetClass) {
    elClass = el.classList;
    elClass.contains(targetClass) ? false : elClass.add(targetClass);
  }
}

function deleteClass(el, targetClass) {
  if (isObj(el) && targetClass) {
    elClass = el.classList;
    elClass.contains(targetClass) ? elClass.remove(targetClass) : false;
  }
}

function modifyClass(el, targetClass) {
  if (isObj(el) && targetClass) {
    elClass = el.classList;
    elClass.contains(targetClass) ? elClass.remove(targetClass) : elClass.add(targetClass);
  }
}

function containsClass(el, targetClass) {
  if (isObj(el) && targetClass && el !== document ) {
    return el.classList.contains(targetClass) ? true : false;
  }
}

function wrapEl(el, wrapper) {
  el.parentNode.insertBefore(wrapper, el);
  wrapper.appendChild(el);
}

function loadSvg(file, parent, path = '/icons/') {
  const link = `{{ absURL "" }}${path}${file}.svg`;
  fetch(link)
  .then((response) => {
    return response.text();
  })
  .then((data) => {
    parent.innerHTML = data;
  });
}

// get and set value from local storage (not cookie)
function getStorValue(key, cookie=null){
  if(key){
    var storageBank;
    if(cookie && typeof cookie == "object"){
      storageBank = cookie;
    } else {
      storageBank = window.localStorage;
    }
    return storageBank.getItem(key);
  } else {
    return undefined
  }
}
function setStorValue(key, value, cookie=null){
  if (key && value){
    var storageBank;
    if (cookie && typeof cookie == "object") {
      storageBank = cookie;
    } else {
      storageBank = window.localStorage;
    }
    storageBank.setItem(key, value);
    return true
  } else {
    return false
  }
}

// get and set value for cookies
function getCookie(name) {
  var nameEQ = name + "=";
  var cookies = document.cookie.split(';');
  for (var i = 0; i < cookies.length; i++) {
    var cookie = cookies[i];
    while (cookie.charAt(0) == ' ') cookie = cookie.substring(1, cookie.length);
    if (cookie.indexOf(nameEQ) == 0) return cookie.substring(nameEQ.length, cookie.length);
  }
  return null;
}
function setCookie(name, value, days) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/;SameSite=Strict";
}
function eraseCookie(name) {
  document.cookie = name + '=; Max-Age=-99999999;';
}

// used in search
// find all indexes
function indexOfAll(query) {
  console.log
  var i = this.indexOf(query),
    indexes = [];
  while (i !== -1) {
    indexes.push(i);
    i = this.indexOf(query, ++i);
  }
  return indexes;
}
  // add method on String prototype
String.prototype.indexOfAll = indexOfAll;
