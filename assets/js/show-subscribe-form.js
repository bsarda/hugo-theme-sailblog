// show or hide subscribe form
function showSubscribeFrom(){
  var subBack = document.getElementsByClassName(classSubscribeDummy).item(0);
  subBack.classList.remove(classSubscribeDummy)
  subBack.classList.add(classSubscribeBack)
  subBack.classList.add(classSubscribeShow)
}
function hideSubscribeFrom(){
  var subBack = document.getElementsByClassName(classSubscribeBack).item(0);
  subBack.classList.add(classSubscribeDummy)
  subBack.classList.remove(classSubscribeBack)
  subBack.classList.remove(classSubscribeShow)
}
// post request
function postSubscribeRequest(mail, name = "", apiEndpoint, apiKey){
  if (apiEndpoint && apiKey){
    var httpRequest = new XMLHttpRequest();
    httpRequest.open("POST", apiEndpoint);
    // set headers
    httpRequest.setRequestHeader("Accept", "application/json");
    httpRequest.setRequestHeader("Content-Type", "application/json");
    httpRequest.setRequestHeader("X-Api-Key", apiKey);
    // prepare to receive on status done (4)
    httpRequest.onreadystatechange = function () {
      if (httpRequest.readyState === 4) {
        // call finished
        console.log(httpRequest.status);
        // console.log(JSON.parse(httpRequest.responseText)["body"]);
      }
    };
    var data = JSON.stringify( {
      "mail": mail,
      "name": name
    } );
    httpRequest.send(data);
  }
}

// add the individual listeners for each button
window.addEventListener('load', function () {
  // get the defaults
  var textField = document.getElementById(dummyIdSubscribeText);
  // on load, pre-cache the to-be-deleted values
  const apiEndpoint = "{{ .Site.Params.subscriptionAPIEndpoint }}"
  const apiKey = "{{ .Site.Params.subscriptionAPIKey }}"
  // if not exists, then we're not on the main page!
  if( textField){
    const textInnerHtml = textField.innerHTML
    const textInnerColor = textField.style.color
    
    const subscribeFromButton = document.getElementsByClassName(classSubscribeButtonForm);
    if (subscribeFromButton && subscribeFromButton.length > 0) {
      subscribeFromButton.item(0).addEventListener('click', function () {
        showSubscribeFrom();
      })
    };

    const buttonCancel = document.getElementsByClassName(classSubscribeButtonCancel)
    if (buttonCancel) {
      buttonCancel.item(0).addEventListener("click", function () {
        // reset to origins
        textField.innerHTML = textInnerHtml
        textField.style.color = textInnerColor
        // hide
        hideSubscribeFrom()
      });
    }

    const buttonSend = document.getElementsByClassName(classSubscribeButtonSend)
    if (buttonSend) {
      buttonSend.item(0).addEventListener("click", function () {
        nameField = document.getElementById(dummyIdSubscribeName)
        emailField = document.getElementById(dummyIdSubscribeEMail)
        subscribeValidated = document.getElementById(dummyIdSubscribeValidated)
        if (emailValidatorRE.test(emailField.value)){
          // hide buttons
          buttonSend.item(0).style.display = "none";
          buttonCancel.item(0).style.display = "none";
          postSubscribeRequest(emailField.value, nameField.value, apiEndpoint, apiKey);
          // once done
          subscribeValidated.style = "display: inherit";
          // 3 seconds after...
          setTimeout(function () {
            // buttons back to normal
            buttonSend.item(0).removeAttribute("style");
            buttonCancel.item(0).removeAttribute("style");
            subscribeValidated.style = "display: none";
            hideSubscribeFrom();
          }, 3000);
        } else {
          // invalid email
          textField = document.getElementById(dummyIdSubscribeText);
          sourceContentInvalid = document.getElementById(dummyIdSubscribeInvalid);
          textField.innerHTML = sourceContentInvalid.innerHTML;
          textField.style.color = "red";
        }
      });
    }
  }
})
