# hugo-theme-sailblog

Theme "Sailblog" for HUGO.

Based on [Hugo Curious](https://github.com/vietanhdev/hugo-curious)
itself based on "clarity" : [Hugo Clarity](https://github.com/vietanhdev/hugo-curious)

Live demo / Used on my blog : [CloudSailor](https://cloudsailor.org)

## Features

* Blog with tagging and category options
* Native Image Lazy Loading
* Customizable (see config)
* Dark Mode (with UI controls for user preference setting)
* Toggleable table of contents
* Zoom on images
* Syntax Highlighting
* Rich code block functions
* Auto TOC using tocbot
* Search page
* Subscription form

## Prerequisites

You need the extended version of Hugo [extended version of Hugo](https://github.com/gohugoio/hugo/releases). See installation steps from [Hugo's official docs](https://gohugo.io/getting-started/installing/).

For subscription you'll need something replying to API POST.

For comments, it includes [Hyvor](https://talk.hyvor.com), register and insert your ID in config.yaml.

## Getting Started

Generate a new Hugo site and add this theme as a Git submodule inside your themes folder:

```bash
hugo new site yourSiteName
cd yourSiteName
git init
git submodule add https://gitlab.com/bsarda/hugo-theme-sailblog themes/sailblog
cp -a themes/hugo-curious/exampleSite/* .
```

Then run

```bash
hugo server
```

## Configuration 'config.yaml'

Open the `config.yaml` file and start configuration your site.

### Global Parameters

These options set global values that some pages or all pages in the site use by default.

| Parameter | Value Type | Usage |
|:---- | ---- | ---- |
| baseUrl | string | url used for all reference to absolute path |
| title | string | title of the blog |
| theme | string | set to the name of the folder, ideally 'sailblog' |
| paginate | string | number of posts per page in excerpt home/post view. defaults to 8. |
| taxonomies | array/string | do not change anything, it's a reference of taxonomies to get/show. |
| pygmentsCodefences | boolean | use code fences to delimit style. Keep it as "true" |
| pygmentsStyle | string | theme for code highlight |
| markup>highlight>lineNos | boolean | number code lines on code highlights |

### Site Parameters

This is the site parameters, under the 'params:' section.

| Parameter | Value Type | Default/Usual value | Usage |
|:---- | ---- | ---- | ---- |
| description | string | none | Description of the blog. Will be used in the meta. |
| copyrightExtended | string | none | sentence to be added in the footer as additional copyright (i.e. show MIT or Apache (c) ) |
| subscriptionAPIEndpoint | string | none | api url for subscription |
| subscriptionAPIKey | string | none | api key used for subscription |
| comment | boolean | true | use comments. If not set, will defaults to true. |
| hyvorTalkWebsiteId | number | none | hyvor key (4 digits) for your user |
| numberOfTaxonomiesShownSidebar | number | 12 | maximum number of tags, categories or series to list in the sidebar (then a button show all will be displayed) |
| numberOfTagsInCard | number | 6 | maximum number of tags to list in a card excerpt |
| numberOfCategoriesInCard | number | 3 | maximum number of categories to list in a card excerpt |
| logoIcon | string | /images/brand/icon.svg | path to the icon of the site. Used in the navbar header. Will use default if not set. |
| logoTitle | string | /images/brand/title.svg | path to the title of the site. Used in the navbar header. Will use default if not set. |
| languageMenuName | string | 🌐 | string used for language menu in the navbar header. |
| enforceMode | string | none | enforce a default style for users, either "light" or "dark" (or any other would be added in future) |
| titleSeparator | string | \| | separator used in page's title in the browser, like 'Home \| Cloudsailor' |
| showCookieConsent | boolean | true | show the cookie/privacy acceptance on first visit and for x days |
| cookieValidityDays | number | 365 | time for the privacy acceptance is valid. Defaults to one year, means users will have to re-validate every year (or less if they clean their cache before) |
| privacyPolicyPage | string | /privacy-policy/ | privacy policy page to show, used in the link for cookie acceptance. |

### Menus

To add, remove, or reorganize top menu items, edit the section in the config file.

```yaml
defaultContentLanguage: en
languages:
  en:
    languageName: English
    contentDir: content/en
    params:
      dateFormat: Jan 2, 2006
    weight: 1
    menu:
      main:
      - name: Posts
        url: /posts
        weight: 10
      - name: About
        url: /about/
        weight: 20
      - name: 🔍
        url: /search/
        weight: 30
  fr:
    languageName: Francais
    contentDir: content/fr
    params:
      dateFormat: 2 Jan 2006
    weight: 1
    menu:
      main:
      - name: Posts
        url: /posts
        weight: 10
      - name: A Propos
        url: /about/
        weight: 20
      - name: 🔍
        url: /search/
        weight: 30
```

### Output Parameters

Leave those parameters as is, it's configured with the minimum required and possible.

| Parameter | Value Type | Usage |
|:---- | ---- | ---- |
| outputs | array/string | don't touch, html is mandatory and json is used for search |

## Page Parameters

These options can be set from a page [frontmatter](https://gohugo.io/content-management/front-matter#readout) or via [archetypes](https://gohugo.io/content-management/archetypes/#readout).

| Parameter | Value Type | Usage |
|:---- | ---- | ---- |
| title | string | title of the post |
| date | date | date released |
| description | string | summary/description. Keep it under 260 chars |
| draft | boolean | draft mode (not published) |
| tags | array/string | tags associated |
| categories | array/string | categories associated |
| series | array/string | series associated |
| toc | boolean | build and show toc on page |
| thumbnail | file path (string) | thumbnail for the excerpt. If not set, defaults to static/images/thumbnails/\<path-of-post\> png or jpg |
| featureImage | file path (string) | featured image (on the top of the post). If not set, defaults to static/images/thumbnails/\<path-of-post\> png or jpg |

## Social media

To edit your social media profile links (on the right of footer bar), edit '/data/social.yaml'

## Web site analytics

Due to filtering and complaints with analytics such as Google Analytics, there is NO analytics included.

You could build your own by your CDN access logs.

## Table of contents

Each article can optionally have a table of contents (TOC) generated for it based on top-level links.

By configuring the `toc` parameter in the article frontmatter and setting it to `true`, a TOC will be generated only for that article. The TOC will then render under the featured image.

### i18n

This theme supports Multilingual (i18n / internationalization / translations)

The `exampleSite` gives you some examples already.
You may extend the multilingual functionality by following the [official documentation](https://gohugo.io/content-management/multilingual/).

Things to consider in multilingual:

* **supported languages** are configured in [config.yaml](./exampleSite/config.yaml)
* **add new language support** by creating a new file inside [i18n](./i18n/) directory.
  Check for missing translations using `hugo server --i18n-warnings`
* **taxonomy** names (tags, categories, etc...) are translated in [i18n](./i18n/) as well (translate the key)
* **menus** are translated manually in the config files [config.yaml](./exampleSite/config.yaml)
* **menu's languages list** are semi-hardcoded. You may chose another text for the menu entry with [languageMenuName](./exampleSite/config.yaml). Please, do better and create a PR for that.
* **content** must be translated individually. Read the [official documentation](https://gohugo.io/content-management/multilingual/#translate-your-content) for information on how to do it.

**note:** if you do NOT want any translations (thus removing the translations menu entry), then you must not have any translations.

To change the values of translatable text, such as `read_more` or `copyright`, edit the values in the language file you are using in the [`i18n`](i18n) directory.

If you have no such directory, copy the one inside the theme to your root Hugo directory.

## Comments

I used Hyvor to comment. Not free, but no data usage like disqus.

### Subscription form

Subscription is done over a call to an API gateway, with arguments name and mail.  
Implemented on my blog using AWS API Gateway, triggering Lambda function, registering in DynamoDB table and back-processed by scripts when needed.
