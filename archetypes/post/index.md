---
title: "{{ .Name }}"
author: Firstname Lastname
date: {{ .Date }}
description: |
  Description of the article.
  Bear in mind the limit of 260 characters.
toc: true
draft: true
categories:
  - technology
series:
  - series1
tags:
  - tag_name1
---

**Insert your text and headers! (starting by ##).**
Place the thumbnail jpg or png in the folder /static/images/thumbnails/<dir-name>.jpg|.png.
<!-- title: "{{ replace .Name "-" " " | title }}" # Title of the blog post. -->
