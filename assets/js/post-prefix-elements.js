// adding classes to heading nodes
function prefixHeadings(){
  // getting the heading nodes
  var headingNodes = [];
  ['h2', 'h3', 'h4', 'h5', 'h6'].forEach(function(tag){
    const contentPane = document.getElementById(dummyIdArticleRoot);
    if (contentPane) {
      const results = contentPane.getElementsByTagName(tag);
      Array.prototype.push.apply(headingNodes, results);
    }
  });
  // adds the link and icon for copying to the nodes h2-h6
  headingNodes.forEach(function(node){
    var link = document.createElement('a');
    loadSvg('link', link);
    link.className = 'link icon';
    const id = node.getAttribute('id');
    if(id) {
      const current = document.URL;
      link.href = `${current}#${id}`;
      node.appendChild(link);
      pushClass(node, classHLink);
      // if needed to add another class from the h tag
      // pushClass(node, 'magic_'+node.localName+'_happens');
    }
  });
}

// prefix and wrap tables
function prefixTables(){
  const contentPane = document.getElementById(dummyIdArticleRoot);
  if(contentPane){
    const tables = contentPane.querySelectorAll('table');
    if (tables) {
      tables.forEach(function (table) {
        const wrapper = document.createElement('div');
        wrapper.className = classTableWrapper;
        wrapEl(table, wrapper);
        pushClass(wrapper, dummyClassPlaceholderTable)
      });
    }
  }
}

// prefix images
function prefixImages(){
  const contentPane = document.getElementById(dummyIdArticleRoot);
  if(contentPane){
    const imgs = contentPane.querySelectorAll('img');
    if(imgs){
      imgs.forEach(function (img) {
        pushClass(img, classImageScaleContainer);
      });
    }
  }
}

// pre- elements, usually code blocks.
function prefixOrphanedPreElements() {
  const pres = document.querySelectorAll('pre');
  Array.from(pres).forEach(function (pre) {
    if (!containsClass(pre.parentNode, classHighlight)) {
      const preWrapper = document.createElement('div');
      preWrapper.className = classHighlight;
      const outerWrapper = document.createElement('div');
      outerWrapper.className = classHighlightWrap;
      wrapEl(pre, preWrapper);
      wrapEl(preWrapper, outerWrapper);
    }
  })
}

// only when page is fully loaded
window.addEventListener('load', function() {
  prefixHeadings();
  prefixTables();
  prefixImages();
  prefixOrphanedPreElements();
});
