// used to zoom on images
function zoomImage() {  
  // create a clone of the image element
  var clone = this.cloneNode();
  clone.classList.remove(classImageScaleContainer);
  // put the cloned image element in the box
  var imgFront = document.getElementsByClassName(classImageScaleFront).item(0);
  imgFront.innerHTML = "";
  imgFront.appendChild(clone)
  // show the box
  var imgBack = document.getElementsByClassName(classImageScaleBack).item(0);
  imgBack.classList.add(classImageScaleShow)
}

// add the listener click on images
window.addEventListener('load', function () {
  const imgs = doc.getElementsByClassName(classImageScaleContainer);
  if(imgs && imgs.length>0){
    [...imgs].forEach(function (img) {
      img.addEventListener("click", zoomImage);
      // sets the image width if fixed in the url - markdown standard
      const widthInSrc = /\?w=(\d+)/.exec(img.getAttribute('src'))
      if (widthInSrc && widthInSrc.length>1){
        img.width = widthInSrc[1]
      }
    });
    // add the listener on clic on the div used for the zoom
    const imgClassBack = document.getElementsByClassName(classImageScaleBack)
    if(imgClassBack){
      imgClassBack.item(0).addEventListener("click", function () {
        this.classList.remove(classImageScaleShow);
      });
    }
  }
});
