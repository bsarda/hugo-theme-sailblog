---
title: "Code Samples"
date: 2021-12-12
categories: ["Test-Pages"]
author: Benoit S.
tags:
- code
- syntax
description: "This is a sample of codes, to test."
# thumbnail: /images/posts/camera-calib.png
# thumbnail: /en/posts/adas-jetson-nano-software/camera-calib.png
# slug: "/en/posts/adas-jetson-nano-software/"
discussionId: "/posts/adas-jetson-nano-software/"
toc: true
---

## From Markup

### Code block with backticks

```html
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Example HTML5 Document</title>
</head>
<body>
  <p>Test</p>
</body>
</html>
```

### Code block indented with four spaces

    <!doctype html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <title>Example HTML5 Document</title>
    </head>
    <body>
      <p>Test</p>
    </body>
    </html>

### Code block with Hugo's internal highlight shortcode
{{< highlight html >}}
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Example HTML5 Document</title>
</head>
<body>
  <p>Test</p>
</body>
</html>
{{< /highlight >}}

### Diff code block

```diff
[dependencies.bevy]
git = "https://github.com/bevyengine/bevy"
rev = "11f52b8c72fc3a568e8bb4a4cd1f3eb025ac2e13"
- features = ["dynamic"]
+ features = ["jpeg", "dynamic"]
```

## From sites

https://therenegadecoder.com/code/sample-programs-in-every-language/
http://helloworldcollection.de
http://rosettacode.org/wiki/Rosetta_Code

### C++

```cpp
#include <iostream>
 
template<int max, int min> struct bottle_countdown
{
  static const int middle = (min + max)/2;
  static void print()
  {
    bottle_countdown<max, middle+1>::print();
    bottle_countdown<middle, min>::print();
  }
};
 
template<int value> struct bottle_countdown<value, value>
{
  static void print()
  {
    std::cout << value << " bottles of beer on the wall\n"
              << value << " bottles of beer\n"
              << "Take one down, pass it around\n"
              << value-1 << " bottles of beer\n\n";
  }
};
 
int main()
{
  bottle_countdown<100, 1>::print();
  return 0;
}
```

### Java

```java
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
 
public class Beer extends JFrame {
    private int x;
    private JTextArea text;
 
    public static void main(String[] args) {
        new Beer().setVisible(true);
    }
 
    public Beer() {
        x = 99;
 
        JButton take = new JButton("Take one down, pass it around");
        take.addActionListener(this::onTakeClick);
 
        text = new JTextArea(4, 30);
        text.setText(x + " bottles of beer on the wall\n" + x + " bottles of beer");
        text.setEditable(false);
 
        setLayout(new BorderLayout());
        add(text, BorderLayout.CENTER);
        add(take, BorderLayout.PAGE_END);
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
 
    private void onTakeClick(ActionEvent event) {
        JOptionPane.showMessageDialog(null, --x + " bottles of beer on the wall");
        text.setText(x + " bottles of beer on the wall\n" + x + " bottles of beer");
        if (x == 0) {
            dispose();
        }
    }
}
```

### HTML

```html
<HTML>
<!-- Hello World in HTML -->
<HEAD>
<TITLE>Hello World!</TITLE>
</HEAD>
<BODY>
Hello World!
</BODY>
</HTML>
```

### CSS

```
```

### Go

```go
package main
 
import "fmt"
 
func main() {
	bottles := func(i int) string {
		switch i {
		case 0:
			return "No more bottles"
		case 1:
			return "1 bottle"
		default:
			return fmt.Sprintf("%d bottles", i)
		}
	}
 
	for i := 99; i > 0; i-- {
		fmt.Printf("%s of beer on the wall\n", bottles(i))
		fmt.Printf("%s of beer\n", bottles(i))
		fmt.Printf("Take one down, pass it around\n")
		fmt.Printf("%s of beer on the wall\n", bottles(i-1))
	}
}
```

### Javascript

```javascript
var bottles = 99;
var songTemplate  = "{X} bottles of beer on the wall \n" +
                    "{X} bottles of beer \n"+
                    "Take one down, pass it around \n"+
                    "{X-1} bottles of beer on the wall \n";
 
function song(x, txt) {
  return txt.replace(/\{X\}/gi, x).replace(/\{X-1\}/gi, x-1) + (x > 1 ? song(x-1, txt) : "");
}
 
console.log(song(bottles, songTemplate));
```

### Python

```python
'''99 Units of Disposable Asset'''
 
 
from itertools import chain
 
 
# main :: IO ()
def main():
    '''Modalised asset dispersal procedure.'''
 
    # localisation :: (String, String, String)
    localisation = (
        'on the wall',
        'Take one down, pass it around',
        'Better go to the store to buy some more'
    )
 
    print(unlines(map(
        incantation(localisation),
        enumFromThenTo(99)(98)(0)
    )))
 
 
# incantation :: (String, String, String) -> Int -> String
def incantation(localisation):
    '''Versification of asset disposal
       and inventory update.'''
 
    location, distribution, solution = localisation
 
    def inventory(n):
        return unwords([asset(n), location])
    return lambda n: solution if 0 == n else (
        unlines([
            inventory(n),
            asset(n),
            distribution,
            inventory(pred(n))
        ])
    )
 
 
# asset :: Int -> String
def asset(n):
    '''Quantified asset.'''
    def suffix(n):
        return [] if 1 == n else 's'
    return unwords([
        str(n),
        concat(reversed(concat(cons(suffix(n))(["elttob"]))))
    ])
 
 
# GENERIC -------------------------------------------------
 
# concat :: [[a]] -> [a]
# concat :: [String] -> String
def concat(xxs):
    '''The concatenation of all the elements in a list.'''
    xs = list(chain.from_iterable(xxs))
    unit = '' if isinstance(xs, str) else []
    return unit if not xs else (
        ''.join(xs) if isinstance(xs[0], str) else xs
    )
 
 
# cons :: a -> [a] -> [a]
def cons(x):
    '''Construction of a list from x as head,
       and xs as tail.'''
    return lambda xs: [x] + xs if (
        isinstance(xs, list)
    ) else chain([x], xs)
 
 
# enumFromThenTo :: Int -> Int -> Int -> [Int]
def enumFromThenTo(m):
    '''Integer values enumerated from m to n
       with a step defined by nxt-m.'''
    def go(nxt, n):
        d = nxt - m
        return list(range(m, d + n, d))
    return lambda nxt: lambda n: (
        go(nxt, n)
    )
 
 
# pred ::  Enum a => a -> a
def pred(x):
    '''The predecessor of a value. For numeric types, (- 1).'''
    return x - 1 if isinstance(x, int) else (
        chr(ord(x) - 1)
    )
 
 
# unlines :: [String] -> String
def unlines(xs):
    '''A single string derived by the intercalation
       of a list of strings with the newline character.'''
    return '\n'.join(xs)
 
 
# unwords :: [String] -> String
def unwords(xs):
    '''A space-separated string derived from
       a list of words.'''
    return ' '.join(xs)
 
 
if __name__ == '__main__':
    main()
```

### Powershell

```powershell
int i = 99;
void setup() {
  size(200, 140);
}
void draw() {
  background(0);
  text(i + " bottles of beer on the wall\n"
     + i + " bottles of beer\nTake one down, pass it around\n"
 + (i-1) + " bottles of beer on the wall\n\n", 
    10, 20);
  if (frameCount%240==239) next();  // auto-advance every 4 secs
}
void mouseReleased() {
  next();  // manual advance
}
void next() {
  i = max(i-1, 1);  // stop decreasing at 1-0 bottles
}
```

### Rust

```rust
trait Bottles {
	fn bottles_of_beer(&self) -> Self;
	fn on_the_wall(&self);
}
 
impl Bottles for u32 {
	fn bottles_of_beer(&self) -> u32 {
		match *self {
			0 => print!("No bottles of beer"),
			1 => print!("{} bottle of beer", self),
			_ => print!("{} bottles of beer", self)
		}
		*self   // return a number for chaining
	}
 
	fn on_the_wall(&self) {
		println!(" on the wall!");
	}
}
 
fn main() {
	for i in (1..100).rev() {
		i.bottles_of_beer().on_the_wall();
		i.bottles_of_beer();
		println!("\nTake one down, pass it around...");
		(i - 1).bottles_of_beer().on_the_wall();
		println!("-----------------------------------");
	}
}
```

### Shell

```shell
99bottles()
esc::exitapp
 
99bottles(x=99) { 
  ToolTip, % Format("{1:} {2:} of beer on the wall, {1:L} {2:} of beer.{4:}{3:} {2:} of beer on the wall!"
  ,(x?x:"No more")
  ,(x=1?"bottle":"bottles")
  ,(x=1?"no more":x=0?99:x-1)
  ,(x?"`nYou take one down pass it around, ":"`nGo to the store and buy some more, ")),500,300
  sleep 99
  x?99bottles(x-1):return       
}
```

### Batch

```batch
@echo off
setlocal 
:main
for /L %%i in (99,-1,1) do (
	call :verse %%i
)
echo no bottles of beer on the wall
echo no bottles of beer
echo go to the store and buy some more
echo 99 bottles of beer on the wall
echo.
set /p q="Keep drinking? "
if %q% == y goto main
if %q% == Y goto main
goto :eof
 
:verse
call :plural %1 res
echo %res% of beer on the wall 
echo %res% of beer
call :oneit %1 res
echo take %res% down and pass it round
set /a c=%1-1
call :plural %c% res
echo %res% of beer on the wall
echo.
goto :eof
 
:plural
if %1 gtr 1 goto :gtr
if %1 equ 1 goto :equ
set %2=no bottles
goto :eof
:gtr
set %2=%1 bottles
goto :eof
:equ
set %2=1 bottle
goto :eof
 
:oneit
if %1 equ 1 (
	set %2=it
) else (
	set %2=one
)
goto :eof
```

### AutoIt

```autoit
local $bottleNo=99
local $lyrics=" "
 
While $bottleNo<>0
	If $bottleNo=1 Then
		$lyrics&=$bottleNo & " bottles of beer on the wall" & @CRLF
		$lyrics&=$bottleNo & " bottles of beer" & @CRLF
		$lyrics&="Take one down, pass it around" & @CRLF
	Else
		$lyrics&=$bottleNo & " bottles of beer on the wall" & @CRLF
		$lyrics&=$bottleNo & " bottles of beer" & @CRLF
		$lyrics&="Take one down, pass it around" & @CRLF
	EndIf
	If $bottleNo=1 Then
		$lyrics&=$bottleNo-1 & " bottle of beer" & @CRLF
	Else
		$lyrics&=$bottleNo-1 & " bottles of beer" & @CRLF
	EndIf
	$bottleNo-=1
WEnd
MsgBox(1,"99",$lyrics)
```

### Terraform

```
```
