// show menu
function showLangMenu(){
  var langElemSub = document.querySelector('.' + dummyClassElementLangMenu).nextElementSibling;
  deleteClass(langElemSub, navbarHide);
  pushClass(langElemSub, dummyClassShowLangMenu);
}

// used in lang-menu.js but also in hide-navbar.js
// no need to export/import as all the js are merged in one bundle.js
function hideLangMenu() {
  var langElemSub = document.querySelector('.' + dummyClassElementLangMenu).nextElementSibling;
  // purge if click somewhere
  deleteClass(langElemSub, dummyClassShowLangMenu);
  pushClass(langElemSub, navbarHide);
}

// add the event listener on clicks
window.addEventListener('click', function (event) {
  if (event.target.matches('.' + dummyClassElementLangMenu)) {
    event.preventDefault();
    showLangMenu()
  } else {
    hideLangMenu()
  }
});
