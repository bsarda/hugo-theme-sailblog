function pureFadeIn(elem) {
    var el = document.getElementById(elem);
    el.style.opacity = 0;
    (function fade() {
        var val = parseFloat(el.style.opacity);
        if ((val += .02) <= 1) {
            el.style.opacity = val;
            requestAnimationFrame(fade);
        }
    })();
};

function pureFadeOut(elem) {
    var el = document.getElementById(elem);
    el.style.opacity = 1;
    (function fade() {
        if ((el.style.opacity -= .02) >= 0) {
            requestAnimationFrame(fade);
        }
    })();
};

function cookieConsent() {
    // var cookie = getCookie(cookieConsentDismiss)
    const consentDiv = document.getElementById(classCookieContainer);
    if (getCookie(cookieConsentDismiss)) {
        deleteClass(consentDiv, classCookieFlexUI)
    } else {
        pureFadeIn(classCookieContainer);
        const contentToUse = document.getElementById(dummyIdCookieContent);
        consentDiv.innerHTML = contentToUse.innerHTML
        pushClass(consentDiv, classCookieFlexUI)
    }
}

function cookieDismiss() {
    setCookie(cookieConsentDismiss, '1', {{ .Site.Params.cookieValidityDays | default 365 }} );
    pureFadeOut(classCookieContainer);
    window.reload()
}

// call init when window is loaded and not before all the html gets built.
window.addEventListener('load', function () {
    // launch at the beginning
    cookieConsent();
    // add the button listener
    const consentButton = document.getElementById(dummyIdCookieAcceptButton);
    if (consentButton) {
        consentButton.addEventListener('click', function () {
            cookieDismiss();
        })
    };
})
