window.addEventListener('load', function(){
  const pageKind = document.getElementById(dummyId404PageKind);
  if (pageKind && pageKind.innerHTML == '404'){
    const buttonBack = document.getElementById(dummyIdButton404Back);
    if (buttonBack) {
      buttonBack.addEventListener('click', function () {
        window.history.back();
      })
    };

    const buttonRand = document.getElementById(dummyIdButton404Rand);
    if (buttonRand) {
      buttonRand.addEventListener('click', function () {
        const listPosts = document.getElementById(dummyId404PostsList).innerHTML.replace(/\[|\]/g,'').split(" ");
        const post = listPosts[Math.floor(Math.random() * listPosts.length)];
        window.open(post,"_self");
      })
    };
  }
});
