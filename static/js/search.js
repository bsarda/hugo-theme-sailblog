const searchQuery = document.getElementById('js-search-query');
const searchResults = document.getElementById('js-search-results');
const searchWait = document.getElementById('js-search-wait');
// i18n
var i18n_foundInContent, i18n_foundInDesc, i18n_foundInTitle, i18n_noMatch;
// options
const countOfMatchToShow = 2;
const charsDescBefore = 48;
const charsDescAfter = 128;
const charsContentsBefore = 64;
const charsContentsAfter = 256;
// holds our full data json
var jsonObj = new Object();

// fetch a json file by http req it
function fetchJSONFile(path, callback) {
  var startTime = performance.now()
  var httpRequest = new XMLHttpRequest();
  httpRequest.onreadystatechange = function () {
    if (httpRequest.readyState === 4) {
      if (httpRequest.status === 200) {
        var data = JSON.parse(httpRequest.responseText);
        if (callback) {
          var endTime = performance.now()
          console.log(`loading json took ${endTime - startTime} milliseconds`)
          callback(data);
        }
      }
    }
  };
  httpRequest.open('GET', path);
  httpRequest.send();
}

function i18nInit(){
  i18n_foundInContent = document.getElementById('i18n_foundInContent').innerHTML
  i18n_foundInDesc = document.getElementById('i18n_foundInDesc').innerHTML
  i18n_foundInTitle = document.getElementById('i18n_foundInTitle').innerHTML
  i18n_noMatch = document.getElementById('i18n_noMatch').innerHTML
  
}

// execute the query
function executeQuery(query){
  var startTime = performance.now()
  query = query.toLowerCase()
  // placeholders
  findingsList = "";
  // parse each article from json object
  for(let indexArticle in jsonObj){
    // getting title, link, content, description to process
    currentArticleTitle = jsonObj[indexArticle].title;
    currentArticleLink = jsonObj[indexArticle].permalink;
    currentArticleDesc = jsonObj[indexArticle].desc;
    currentArticleContents = jsonObj[indexArticle].contents;

    // look in titles
    indexOfInTitle = currentArticleTitle.toLowerCase().indexOf(query);
    if (indexOfInTitle>=0){
      let extractedFromTitleMatch = currentArticleTitle.substring(indexOfInTitle, indexOfInTitle + query.length);
      let extractedFromTitleBefore = currentArticleTitle.substring(0, indexOfInTitle);
      let extractedFromTitleAfter = currentArticleTitle.substring(indexOfInTitle + query.length, currentArticleTitle.length);
      findingsList += '<div class="' + classSearchFindingsBlock + '"><a href="' + currentArticleLink + '">' + currentArticleTitle + '</a><span class="' + classSearchFindingsLabel + '"> -- ' + i18n_foundInTitle + ' :</span>';
      findingsList += '<div class="' + classSearchFindingsListShow + '">'
      findingsList += '<span class="' + classSearchFindingsPrePostMatch + '"> ' + extractedFromTitleBefore + '<span class="' + classSearchFindingsHighlight + '">' + extractedFromTitleMatch + '</span>' + extractedFromTitleAfter + '</span>'
      findingsList += '</div>'
      findingsList += '</div>';
    }

    // look in desc
    let indexesOfInDesc = currentArticleDesc.toLowerCase().indexOfAll(query);
    if (indexesOfInDesc && indexesOfInDesc.length > 0) {
      showMoreButton = false;
      findingsList += '<div class="' + classSearchFindingsBlock + '"><a href="' + currentArticleLink + '">' + currentArticleTitle + '</a><span class="' + classSearchFindingsLabel + '"> -- ' + i18n_foundInDesc + ' :</span>'
      indexesOfInDesc.forEach(function (indexOfInDesc, iter) {
        let extractedFromDescMatch = currentArticleDesc.substring(indexOfInDesc, indexOfInDesc + query.length);
        let extractedFromDescBefore = currentArticleDesc.substring(indexOfInDesc - charsDescBefore, indexOfInDesc);
        let extractedFromDescAfter = currentArticleDesc.substring(indexOfInDesc + query.length, indexOfInDesc + query.length + charsDescAfter);
        // class to show or hide depending on count
        showLineClass = classSearchFindingsListShow;
        if (iter > countOfMatchToShow - 1) {
          showLineClass = classSearchFindingsListHide;
          showMoreButton = true
        }
        let prefixDots = "...";
        let suffixDots = "...";
        if (indexOfInDesc - charsDescBefore < 0){
          prefixDots = "";
        }
        if (indexOfInDesc + query.length + charsDescAfter >= currentArticleDesc.length) {
          suffixDots = "";
        }
        findingsList += '<div class="' + showLineClass + '"><span class="' + classSearchFindingsPrePostMatch + '">' + (iter + 1) + '] ' + prefixDots + extractedFromDescBefore + '<span class="' + classSearchFindingsHighlight + '">' + extractedFromDescMatch + '</span>' + extractedFromDescAfter + suffixDots + '</span></div>'
      });
      if (showMoreButton) {
        findingsList += '<div class="' + classSearchFindingsShowMore + '" id="' + dummyIdSearchShowMore + '">... show more / collapse</div>'
      }
      findingsList += '</div>'
    }

    // look in contents
    let indexesOfInContents = currentArticleContents.toLowerCase().indexOfAll(query);
    if (indexesOfInContents && indexesOfInContents.length>0) {
      showMoreButton = false;
      findingsList += '<div class="' + classSearchFindingsBlock + '"><a href="' + currentArticleLink + '">' + currentArticleTitle + '</a><span class="' + classSearchFindingsLabel + '"> -- ' + i18n_foundInContent + ' :</span>'
      indexesOfInContents.forEach(function(indexOfInContents, iter) {
        let extractedFromContentsMatch = currentArticleContents.substring(indexOfInContents, indexOfInContents + query.length);
        let extractedFromContentsBefore = currentArticleContents.substring(indexOfInContents - charsContentsBefore, indexOfInContents);
        let extractedFromContentsAfter = currentArticleContents.substring(indexOfInContents + query.length, indexOfInContents + query.length + charsContentsAfter);
        // class to show or hide depending on count
        showLineClass = classSearchFindingsListShow;
        if (iter > countOfMatchToShow-1){
          showLineClass = classSearchFindingsListHide;
          showMoreButton = true
        }
        let prefixDots = "...";
        let suffixDots = "...";
        if (indexOfInContents - charsContentsBefore < 0) {
          prefixDots = "";
        }
        if (indexOfInContents + query.length + charsContentsAfter >= currentArticleContents.length) {
          suffixDots = "";
        }
        findingsList += '<div class="' + showLineClass + '"><span class="' + classSearchFindingsPrePostMatch + '">' + (iter + 1) + '] ' + prefixDots + extractedFromContentsBefore + '<span class="' + classSearchFindingsHighlight + '">' + extractedFromContentsMatch + '</span>' + extractedFromContentsAfter + suffixDots + '</span></div>'
      });
      if (showMoreButton) {
        findingsList += '<div class="' + classSearchFindingsShowMore + '" id="' + dummyIdSearchShowMore + '">... show more / collapse</div>'
      }
      findingsList += '</div>'
    }

  }
  if (!findingsList){
    searchResults.innerHTML = i18n_noMatch;
  } else {
    searchResults.innerHTML = findingsList;
  }
  var endTime = performance.now()
  console.log(`searching took ${endTime - startTime} milliseconds`)
}

// clean the result pane
function cleanResults(){
  searchResults.innerHTML = ""
  searchResults.style.visibility = "visible";
}

// ===============================
// at launch - even before waiting listener load finished
// load the data
fetchJSONFile('/index.json', function (data) {
  jsonObj = data;
});

window.addEventListener('load', function () {
  // init the i18n - not before page is fully loaded..
  i18nInit();
  // clean the value of search field - especially when hitting back button
  searchQuery.value = "";
  // search input listener (requires incremental)
  searchQuery.addEventListener('search', function () {
    if (searchQuery.value.length >= 3) {
      // show the loading animation
      searchWait.style.display='block';
      // clean text
      cleanResults()
      // sleep after execute query
      // setTimeout(() => {
      //   executeQuery(searchQuery.value);
      //   searchWait.removeAttribute('style');
      // }, 500);
      executeQuery(searchQuery.value);
      // remove animation
      searchWait.removeAttribute('style');

    } else if (searchQuery.value.length < 3) {
      cleanResults()
    }
  });

  // listen for click on "show more" button
  searchResults.addEventListener('click', function(event) {
    // it's a show more button
    if (event.target.id == dummyIdSearchShowMore){
      // get the child nodes and process hide/unhide
      let child = event.target.parentElement.childNodes;
      child.forEach(function(node){
        if (node.classList && containsClass(node, classSearchFindingsListHide)){
          deleteClass(node, classSearchFindingsListHide)
          pushClass(node, dummyIdSearchDummyExtend)
          pushClass(node, classSearchFindingsListShow)
        } else if (node.classList && containsClass(node, dummyIdSearchDummyExtend)) {
          pushClass(node, classSearchFindingsListHide)
          deleteClass(node, classSearchFindingsListShow)
          deleteClass(node, dummyIdSearchDummyExtend)
        }
      });
    }
  });
});