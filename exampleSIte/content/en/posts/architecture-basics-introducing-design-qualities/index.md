---
title: "Architecture Basics: Introducing Design Qualities"
author: Benoit Sarda
date: 2021-01-04
description: |
  Enterprise Architecture is large and complex, let’s start by focusing on the technical points:  
  A series of “back to basics” articles begins with this introduction to understand where Design Qualities come from, before going deeper into the most important qualities and why it matters.
toc: true
categories: 
  - architecture
tags: 
  - design-qualities
  - method
  - amprs
  - ramps
  - spammers
series:
  - archi-basics
---

## Opening Words

Enterprise Architecture is large and complex, let's start by focusing on the technical points:  
A series of _"back to basics"_ articles begins with this introduction to understand where **Design Qualities** come from, before going deeper into the most important qualities and why it matters.

This comes from the toolbox available to architects, made up of frameworks, methods but above all experience. The role of an architect is not to master the frameworks, but to use it effectively: it is a tool, not a goal.

## Start of a Design Process

Any Design starts with requirements gathering. Before starting a Solution Design, the needs and requirements of the customer (internal or external) must be captured and tracked.

There are several ways to do this, the most common being to use a spreadsheet called **Requirements Traceability Matrix (RTM)**, it's a well-known document to project managers but also Enterprise Architects initiated to frameworks.

What is captured in this RTM are **Functional** and **Non-Functional** requirements. Generally, a Non-Functional (called Technical sometimes) is connected to **Design Qualities** because it arises from business drivers that are measurable - similarly, these Design Qualities are measurable and key to the implementation of the systems.

![](/images/posts/architecture-basics-introducing-design-qualities/nfr-to-dq.png?w=1024)

## But what are these Design Qualities?

There are many Qualities that exist, as listed on the wiki page: [List of Quality Attributes in Wikipedia](https://en.wikipedia.org/wiki/List_of_system_quality_attributes).

Some are used more for software development, others for computer science. Generally in IT Infrastructure we use 5-8 qualities during a design process.  
It is common to find the list of qualities as an acronym, for example at VMware we use RAMPS (or AMPRS) or the most recent and comprehensive **SPAMMERS**.

![](/images/posts/architecture-basics-introducing-design-qualities/spammers.png?w=1024)

## Why this is important?

Design Qualities enables a common and simple language to solve the problems related to the technology.

They are used when moving from conceptual model to logical and physical models:

![](/images/posts/architecture-basics-introducing-design-qualities/c2l.png?w=1002)

This helps to **choose the right directions** for the solution and makes it possible to **justify the Design Decision**. Again, this is part of a framework, a set of tools - it helps to align.

Depending on the choice of Design Qualities you have made, this can also help you in the architecture for **day2**. In the SPAMMERS, 50% of qualities are for day2: Availability, Maintainability, Manageability, Recoverability are required Operational needs: it serves the business by **keeping the systems usable and easy to maintain**, while others add metrics to monitor.

![](/images/posts/architecture-basics-introducing-design-qualities/spammers-day2.png?w=1024)

Designing a system must take into account the use and operationalization for the coming years (3 to 5 years in the case of infrastructure). Design Decisions must then take into account the dimensions of time, implementation and ease of maintenance.

## Final Words

This introduction helps to set up the scene. Certain quality attributes are known, sometimes without realizing it, but often forgotten in the link between the solution, the requirements and the design document bundle.

However, we could see that the **Design Qualities** were an **essential tool** to constitute a correct and qualitative design.

The next article will address one of the Design Qualities: Availability. Stay tuned !
