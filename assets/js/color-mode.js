function getCustomizationColorModeStorCurrent() {
    var mode = getStorValue(storKeyColorMode);
    if (!mode) {
        mode = getComputedStyle(document.documentElement).getPropertyValue(htmlColorModeKey).replace(/\"/g, '').trim();
    }
    return mode
}

// sets the customization to boolean value (or switch if no value given) - color
function setCustomizationColorMode(status) {
    if (typeof status == "undefined" || (typeof status != "boolean" && typeof status != "string") ) {
        const mode = getCustomizationColorModeStorCurrent();
        if (mode && mode == storValue1ColorMode){
            document.documentElement.setAttribute(htmlColorModeData, storValue2ColorMode);
            setStorValue(storKeyColorMode, storValue2ColorMode)
        } else if (mode && mode == storValue2ColorMode) {
            document.documentElement.setAttribute(htmlColorModeData, storValue1ColorMode);
            setStorValue(storKeyColorMode, storValue1ColorMode)
        } else {
            // current status not found
        }
    } else if (status == true) {
        document.documentElement.setAttribute(htmlColorModeData, storValue2ColorMode);
        setStorValue(storKeyColorMode, storValue2ColorMode)
    } else if (status == false) {
        document.documentElement.setAttribute(htmlColorModeData, storValue1ColorMode);
        setStorValue(storKeyColorMode, storValue1ColorMode)
    } else if (typeof status == "string") {
        document.documentElement.setAttribute(htmlColorModeData, status);
        setStorValue(storKeyColorMode, status)
    } else {
        // do nothing
    }
}
// init at first launch or page refresh
function initCustomizationColorMode(){
    const mode = getCustomizationColorModeStorCurrent();
    currentMode = getComputedStyle(doc).getPropertyValue(htmlColorModeKey);
    setCustomizationColorMode(mode);
}

initCustomizationColorMode();

doc.addEventListener('click', function (event) {
    let target = event.target;
    if (target.matches('.' + dummyClassSwitchColor)){
        pushClass(target, classColorAnimate);
        setCustomizationColorMode();
    }
});