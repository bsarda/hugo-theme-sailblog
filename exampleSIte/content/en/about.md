---
title: About
author: Firstname Lastname
description: About this blog - you want to know more? visit this page!
discussionId: "/about/"
date: 2020-10-10
toc: true
categories: 
  - "introduction"
tags: 
  - "about"
# series:
---

A classic about page.