// Auto hide navbar when scrolling down
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById(classNavbar).style.top = "0";
  } else {
    // hide navbar
    document.getElementById(classNavbar).style.top = "-51px";
    // hide lang select, in case of
    hideLangMenu()
  }
  prevScrollpos = currentScrollPos;
}